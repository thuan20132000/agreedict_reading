<?php

namespace App\Http\Controllers;

use App\Model\Post;
use App\Model\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $topics = Topic::orderBy('created_at','desc')->paginate(10);
        return view('Admin.Topic.index',['topics'=>$topics]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Admin.Topic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=>'required'
        ],[
            'name.required'=>'Please Enter Name'
        ]);

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'topic-'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
                return redirect()->back()->with('failed','Create Fialed');
            }
        }

        $topic = new Topic();
        $topic->name = $request->name;
        $topic->slug = Str::slug($request->name,'-');
        $topic->image = $fileName;
        $topic->save();

        return redirect()->back()->with('success','Created Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        //
        return view('Admin.Topic.edit',['topic'=>$topic]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topic $topic)
    {
        //
        $request->validate([
            'name'=>'required'
        ],[
            'name.required'=>'Please Enter Name'
        ]);

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'topic-'.time().$request->image->extension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                return redirect()->back()->with('failed','Update Failed');
            }
        }

        $topic->name = $request->name;
        $topic->slug = Str::slug($request->name,'-');
        if($request->image){
            $topic->image = $fileName;
        }
        $topic->save();

        return redirect()->back()->with('success','Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic)
    {
        //
        try {
            //code...
            Post::where('topic_id',$topic->id)->delete();
            Topic::where('id',$topic->id)->delete();
        } catch (\Throwable $th) {
            return redirect()->back()->with('failed',$th->getMessage());
        }

        return redirect()->back()->with('success','Delete Successfully');
    }
}
