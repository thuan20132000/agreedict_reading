<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticationController extends Controller
{
    //


    public function register(){
        return view('Admin.Authentication.register');
    }
    public function handleRegister(Request $request){
        dd($request->all());
    }

    public function login(){
        return view('Admin.Authentication.login');
    }

    public function handleLogin(Request $request){

        $request->validate([
            'email'=>'required',
            'password'=>'required'
        ],[
            'email.required'=>"Please Enter Email...",
            'password.required'=>"Please Enter Password..."
        ]);

        try {
            $checkAuth  = Auth::attempt(['email' => $request->email, 'password' => $request->password]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        dd($checkAuth);
    }

    public function logout(){
        return redirect()->route('admin.auth.login');
    }
}
