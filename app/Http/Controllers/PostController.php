<?php

namespace App\Http\Controllers;

use App\Model\Post;
use App\Model\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends Controller
{

    public $topic_id;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $posts = Post::orderBy('created_at','desc')->paginate(20);
        $topics = Topic::all();

        if($topic_id = $request->input('topic')){
            $posts = Post::where('topic_id',$topic_id)->orderBy('created_at','desc')->paginate(10);
        }

        return view('Admin.Post.index',compact(['posts','topics']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $topics = Topic::all();
        return view('Admin.Post.create',['topics'=>$topics]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=>'required',
            'content'=>'required'
        ],[
            'name.required'=>'Please Enter Name',
            'content.required'=>'Please enter content'
        ]);

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'post-'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                throw $th;
                return redirect()->back()->with('failed','Create Fialed');
            }
        }

        $post = new Post();
        $post->topic_id = $request->topic_id;
        $post->slug = Str::slug($request->name,'-');
        $post->name = $request->name;
        $post->description = $request->description;
        $post->content = $request->content;
        $post->image = $fileName;
        $post->save();

        return redirect()->back()->with('success','Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
        $topics = Topic::all();
        return view('Admin.Post.edit',['post'=>$post,'topics'=>$topics]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
        $request->validate([
            'name'=>'required',
            'content'=>'required'
        ],[
            'name.required'=>'Please Enter Name',
            'content.required'=>'Please enter content'
        ]);

        $fileName = 'default.jpg';
        if ($request->image) {
            try {
                $fileName = 'post-'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads'), $fileName);
            } catch (\Throwable $th) {
                return redirect()->back()->with('failed',$th->getMessage());
            }
        }
        $post->topic_id = $request->topic_id;
        $post->slug = Str::slug($request->name,'-');
        $post->name = $request->name;
        $post->description = $request->description;
        $post->content = $request->content;
        if($request->image){
            $post->image = $fileName;
        }
        $post->update();

        return redirect()->back()->with('success','Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
        try {
            $post->delete();
        } catch (\Throwable $th) {
            return redirect()->back()->with('failed',$th->getMessage());
        }

        return redirect()->back()->with('success','Delete successfully');

    }
}
