<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    //

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
