<?php

use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('register', 'AuthenticationController@register')->name('admin.auth.register');
Route::post('register', 'AuthenticationController@handleRegister')->name('admin.auth.register');

Route::get('logout','AuthenticationController@logout')->name('admin.auth.logout');

Route::get('login','AuthenticationController@login')->name('admin.auth.login');
Route::post('login','AuthenticationController@handleLogin')->name('admin.auth.login');


Route::group(['prefix' => 'admin'], function () {


    Route::get('/', function () {
        return view('Admin.index');
    })->name('admin.dashboard');


    /**
     * author: Thuantruong
     *
     */
    Route::resource('/topic', 'TopicController');

    /**
     * author: Thuantruong
     *
     */
    Route::resource('/post', 'PostController');




});


