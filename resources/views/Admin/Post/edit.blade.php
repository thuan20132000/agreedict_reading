@extends('Admin.layouts.master')


<script src="{{ asset('ckfinder/ckfinder.js') }}"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>

@section('content')
@include('Admin.layouts.sidebar')
@include('Admin.layouts.topbar')

<div class="container-fluid">
    <div>
        @if(session()->has('success'))
        <div class="alert alert-success" >
            {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('failed'))
        <div class="alert alert-danger" >
            {{ session()->get('failed') }}
        </div>
        @endif
    </div>

    <!-- Default form contact -->
<form class="text-center border border-light p-5" method="POST" action="{{ route('post.update',$post)}}" enctype="multipart/form-data" >
    @csrf
    @method('PUT')
    <!-- Name -->
    <div class="row">
        <div class="col">
            <select name="topic_id" class="custom-select custom-select-md mb-4">
                @foreach ($topics as $topic)
                    @if ($topic->id == $post->topic->id)
                        <option selected value="{{$topic->id}}">{{$topic->name}}</option>
                    @else
                        <option value="{{$topic->id}}">{{$topic->name}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="col">
            <input name="name" value="{{$post->name}}" type="text" id="defaultContactFormName" class="form-control mb-4 @error('name') is-invalid @enderror" placeholder="Name">

        </div>
    </div>
    <!--Textarea with icon prefix-->
    <div class="md-form amber-textarea mb-4">
        <label class="float-left" for="">Description</label>
        <textarea name="description" id="form24" class="md-textarea form-control" rows="3">
            {{ $post->description }}
        </textarea>
    </div>
    <div class="custom-file form-control mb-4" id="uploadForm">
        <input name="image" id="file" type="file" class="custom-file-input" id="customFileLang" lang="pl-Pl">
        <label class="custom-file-label" for="customFileLang">Upload Image</label>
    </div>
    <img src="{{ $post->image?asset('uploads/'.$post->image):asset('uploads/default.jpg') }}" class="mb-4" width="200" height="200" alt="" srcset="">

    <textarea name="content" id="editor" class="mb-4">
            {!! $post->content !!}
    </textarea>
    <br>
    <!-- Send button -->
    <button class="btn btn-info btn-block" type="submit">Update</button>

</form>
<!-- Default form contact -->

</div>

    <script type="text/javascript">

            CKEDITOR.replace( 'editor',{
                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserWindowWidth: '1000',
                filebrowserWindowHeight: '700'
            });

    </script>

@endsection
