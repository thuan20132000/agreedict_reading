@extends('Admin.layouts.master')

@section('content')
@include('Admin.layouts.sidebar')
@include('Admin.layouts.topbar')

<div class="container-fluid">

      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
      </div>
      <div>
        @if(session()->has('success'))
        <div class="alert alert-success" >
            {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('failed'))
        <div class="alert alert-danger" >
            {{ session()->get('failed') }}
        </div>
        @endif
    </div>

      <div class="d-flex flex-row-reverse bd-highlight" >
        <a href="{{ route('post.create')}}" type="button" class="btn btn-primary">Add New</a>

      </div>
      <table class="table">
        <caption>List of Post</caption>
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">
                <select name="" id="topic_display" onchange="myFunction()">
                    <option value="" selected>TOPIC</option>
                    @foreach ($topics as $topic)
                        <option value="{{$topic->id}}">{{$topic->name}}</option>
                    @endforeach
                </select>
                <script>
                    function myFunction(){
                        let idtp = document.getElementById('topic_display').value;
                        location.href = location.href + `?&topic=${idtp}`;
                    }
                </script>

            </th>
            <th scope="col">Slug</th>
            <th scope="col">Viewer</th>
            <th scope="col">Image</th>
            <th scope="col">Settings</th>

          </tr>
        </thead>
        <tbody>
            @foreach ($posts as $key => $post)
                <tr>
                    <th scope="row">{{$key}}</th>
                    <td>{{$post->name}}</td>
                    <td><a href="{{ route('post.index', ['topic'=>$post->topic->id]) }}" class="btn btn-outline-primary">{{$post->topic->name}}</a></td>
                    <td>{{$post->slug}}</td>
                    <td>{{$post->viewer}}</td>
                    <td>
                        <img src="{{ $post->image?asset('uploads/'.$post->image):asset('uploads/default.jpg') }}" width="100" height="100" alt="" sizes="100" srcset="">
                    </td>
                    <td>
                        <a href="{{ route('post.edit',$post) }}" class="btn btn-primary"><i class="fas fa-edit "></i></a>
                        <a href="#" onclick="document.getElementById('formDelete').submit()" class="btn btn-danger"><i class="fas fa-trash-alt "></i></a>
                        <form hidden id="formDelete" action="{{ route('post.destroy',$post) }}" method="post">
                            @csrf
                            @method('DELETE')
                        </form>
                    </td>
                </tr>
            @endforeach


        </tbody>
      </table>
      <div style="display:flex;justify-content: center">
          {{$posts->links()}}
      </div>
</div>

@endsection
