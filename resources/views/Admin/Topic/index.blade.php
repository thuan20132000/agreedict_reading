@extends('Admin.layouts.master')

@section('content')
@include('Admin.layouts.sidebar')
@include('Admin.layouts.topbar')

<div class="container-fluid">

      <!-- Page Heading -->
      <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
      </div>
      <div>
        @if(session()->has('success'))
        <div class="alert alert-success" >
            {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('failed'))
        <div class="alert alert-danger" >
            {{ session()->get('failed') }}
        </div>
        @endif
    </div>

      <div class="d-flex flex-row-reverse bd-highlight" >
        <a href="{{ route('topic.create')}}" type="button" class="btn btn-primary">Add New</a>

      </div>
      <table class="table">
        <caption>List of Topic</caption>
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Slug</th>
            <th scope="col">Image</th>
            <th scope="col">Settings</th>

          </tr>
        </thead>
        <tbody>
            @foreach ($topics as $key => $topic)
                <tr>
                    <th scope="row">{{$key}}</th>
                    <td>{{$topic->name}}</td>
                    <td>{{$topic->slug}}</td>
                    <td>
                        <img src={{ $topic->image?asset('uploads/'.$topic->image):asset('uploads/default.jpg') }} width="100" height="100"  alt="" width srcset="">
                    </td>
                    <td>
                        <a href="{{ route('topic.edit',$topic) }}" class="btn btn-primary"><i class="fas fa-edit "></i></a>

                        <a href="#" onclick="document.getElementById('formDelete').submit()" class="btn btn-danger"><i class="fas fa-trash-alt "></i></a>
                        <form hidden id="formDelete" action="{{ route('topic.destroy',$topic) }}" method="post">
                            @csrf
                            @method('DELETE')
                        </form>
                    </td>
                </tr>
            @endforeach


        </tbody>
      </table>

</div>

@endsection
