@extends('Admin.layouts.master')

@section('content')
@include('Admin.layouts.sidebar')
@include('Admin.layouts.topbar')

<div class="container-fluid">
    <div>
        @if(session()->has('success'))
        <div class="alert alert-success" >
            {{ session()->get('success') }}
        </div>
        @endif
        @if(session()->has('failed'))
        <div class="alert alert-danger" >
            {{ session()->get('failed') }}
        </div>
        @endif
    </div>

    <!-- Default form contact -->
<form class="text-center border border-light p-5" method="POST" action="{{ route('topic.store')}}" enctype="multipart/form-data" >
    @csrf
    <!-- Name -->

    <input name="name" value="{{old('name')}}" type="text" id="defaultContactFormName" class="form-control mb-4 @error('name') is-invalid @enderror" placeholder="Name">
    <div class="custom-file form-control mb-4" id="uploadForm">
        <input name="image" id="file" type="file" class="custom-file-input" id="customFileLang" lang="pl-Pl">
        <label class="custom-file-label" for="customFileLang">Upload Image</label>
    </div>

    <!-- Send button -->
    <button class="btn btn-info btn-block" type="submit">Create</button>

</form>
<!-- Default form contact -->

</div>


@endsection
