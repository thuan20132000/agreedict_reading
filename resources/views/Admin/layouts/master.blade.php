@include('Admin.layouts.head')

 <!-- Begin Page Content -->

    @yield('content')

<!-- End of Page Wrapper -->

@include('Admin.layouts.footer')
