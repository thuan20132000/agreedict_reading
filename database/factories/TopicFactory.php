<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Topic;
use Faker\Generator as Faker;

$factory->define(Topic::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->word(),
        'slug'=>$faker->slug(),
    ];
});
