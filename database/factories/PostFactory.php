<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Post;
use App\Model\Topic;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        //
        'name'=>$faker->word(),
        'slug'=>$faker->slug(),
        'description'=>$faker->paragraph(),
        'content'=>$faker->paragraph(),
        'topic_id'=>function(){
            return Topic::all()->random();
        }

    ];
});
