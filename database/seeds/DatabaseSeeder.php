<?php

use App\Model\Post;
use Illuminate\Database\Seeder;
use App\Model\Topic;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        factory(Topic::class,10)->create();
        factory(Post::class,50)->create();

    }
}
